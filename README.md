# Quasimorphisms

This is a basic formalisation of quasimorphisms in the
proof assistant Lean.

## License

Copyright (c) 2022 Clara Löh. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Author: Clara Löh.


