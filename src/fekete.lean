/-
Copyright (c) 2022 Clara Löh. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Author: Clara Löh.
-/

import data.real.basic
import topology.basic
import topology.instances.real
import order.filter
import analysis.subadditive

import lim_basics

/-!

# The Fekete lemma and a quasi-version of the Fekete lemma

We formalise and prove the Fekete lemma as well as a quasi-version 
of it that is convenient in many variants of group theory (e.g., 
for homogenisations of quasimorphisms or for stable commutator 
length).

This is partially based on the Isabelle/HOL version Fekete.thy 
by Sébastien Gouëzel:

  https://www.isa-afp.org/browser_info/current/AFP/Ergodic_Theory/Fekete.html

using the classical Fekete lemma in analysis.subadditive .  
-/

/-
## The classical version
-/

/- Fekete's lemma takes sub-additive sequences as input. 

   We rewrite Fekete's lemma from analysis.subadditive 
   in our terminology.
-/

-- This is the same as analysis.subadditive
def is_subadditive
    (a : nat → real)
:= ∀ n m : nat, a (n+m) ≤ a n + a m

def is_boundedbelow
    (a : nat → real)
:= ∃ c : real, ∀ n : nat, n > 0 → a n ≥ c

lemma boundedbelow_via_set 
    (a : nat → real)
    (a_bddbelow: is_boundedbelow a)
  : bdd_below (set.range (λ (n:ℕ), a n))
:=
begin
  -- From is_boundedbelow we obtain a lower bound for _all_ members of the sequence:
  rcases a_bddbelow with ⟨ c, c_est⟩,
  have bddbelow_allnat : ∀ n : ℕ, min c (a 0) ≤ a n, from 
  begin 
    assume n : ℕ,
    -- we distinguish two cases: being zero or positive:
    cases n,
    -- n = 0
    {simp},
    -- n.succ > 0
    {have nsucc_pos : n.succ > 0, by simp, 
     have c_leq_ansucc : c ≤ a n.succ, by exact c_est n.succ nsucc_pos, 
     show _, by exact min_le_of_left_le (c_est (nat.succ n) nsucc_pos)}
  end,  

  -- Then, this lower bound can be converted into the claimed bound:
  show _, 
       by {use min c (a 0),
           simp only [lower_bounds, set.mem_set_of_eq, set.forall_range_iff],
           exact bddbelow_allnat},
end      

-- In this terminology, the classical Fekete lemma can be stated as follows:
theorem fekete
        (a : nat → real)
        (a_subadditive: is_subadditive a)
        (a_bddbelow: is_boundedbelow (λ n : nat, a n/n))
      : is_convergent (λ n : nat, a n/n)
      ∧ (∀ k : nat, k > 0 → a k ≥ k * lim filter.at_top (λ n: nat, a n / n))
:=
begin
  -- We rewrite the boundedness assumption:
  let a_bddbelow' := boundedbelow_via_set (λ n : nat, a n/↑n) a_bddbelow,

  -- More specifically, the limit will be given by the infimum:
  -- (by definition, subadditive.lim is the inf of (a n / n)_n )
  let b : real := subadditive.lim a_subadditive, 
  have avg_tendsto_b : filter.tendsto (λ (n : ℕ), a n / ↑n) filter.at_top (nhds b), 
       by exact subadditive.tendsto_lim a_subadditive a_bddbelow',
  have avg_lim_is_b : lim filter.at_top (λ n : ℕ,  a n/n) = b, 
       by exact tendsto_lim (λ n : ℕ, a n/n) b avg_tendsto_b,

  have part_1 : is_convergent (λ n : nat, a n /n), from
  begin
    use b,
    exact avg_tendsto_b,
  end,

  have part_2 : ∀ k : nat, k > 0 → a k ≥ k * lim filter.at_top (λ n : nat, a n / n), from
  begin
    assume k : nat,
    assume k_pos: k > 0,
    have k_neq_0 : k ≠ 0, by linarith,
    have k_pos' : 0 < (k : ℝ), by exact nat.cast_pos.mpr k_pos,

    have b_leq_avg : b ≤ a k / ↑k, 
         by exact subadditive.lim_le_div a_subadditive a_bddbelow' k_neq_0,

    calc ↑k * lim filter.at_top (λ n : nat, a n / n) 
           = ↑k * b : by rw[avg_lim_is_b]
       ... ≤ a k    : by exact (le_div_iff' k_pos').mp b_leq_avg,
  end,

  show _, by exact ⟨part_1, part_2⟩,
end          

/-
## The quasi-version
-/

/- The quasi-version of Fekete's lemma applies to quasi-subadditive sequences. -/
def is_quasi_subadditive
    (a : nat → real)
:= ∃ c : real, ∀ n m : nat, abs ( a (n+m) - a n - a m) ≤ c


/- We first derive some basic properties of quasi-subadditive sequences and 
   then derive the quasi-version of Fekete's lemma from the classical 
   Fekete lemma. In fact, we prove two quantitative versions of the quasi-version 
   and one that only asserts convergence of the averages. -/
lemma qsubadd_without_abs
      (a : nat → real)
      (c : real)
      (a_qsubadd: ∀ n m : nat, abs (a (n+m) - a n - a m) ≤ c)
    : (∀ n m : nat, a (n+m) ≥ a n + a m - c)
    ∧ (∀ n m : nat, a (n+m) ≤ a n + a m + c)
:=
begin
  have part_1: ∀ n m : nat, a (n+m) ≥ a n + a m - c, from
  begin
    assume n m : nat,
    -- a rearranged version of the claim that is easier to handle
    have claim_minus : - a(n+m) + a n + a m ≤ c, from
         calc - a(n+m) + a n + a m 
                  = - ( a(n+m) -  a n - a m)   : by linarith
              ... ≤ abs ( a (n+m) - a n - a m) : by exact neg_le_abs_self (a (n+m) - a n - a m) 
              ... ≤ c                          : by apply a_qsubadd,
    -- and the claimed inequality
    calc a (n+m) ≥ a n + a m - c : by {simp[claim_minus], linarith},
  end,

  have part_2: ∀ n m : nat, a(n+m) ≤ a n + a m + c, from
  begin 
    assume n m : nat, 
    -- a rearranged version of the claim
    have claim_plus : a(n+m) - a n - a m ≤ c, from
         calc a(n+m) - a n - a m
                ≤ abs ( a (n+m) - a n - a m) : by exact le_abs_self (a (n + m) - a n - a m)
            ... ≤ c                          : by apply a_qsubadd,
    -- and the claimed inequality
    calc a (n+m) ≤ a n + a m + c : by linarith,
  end,  

  show _, by exact ⟨part_1, part_2⟩,
end    

/- The normalised sequence of a quasi-subadditive sequence 
   is bounded below. -/
lemma qsubadd_bddbelow
      (a : nat → real)
      (a_qsubadditive: is_quasi_subadditive a)
    : (is_boundedbelow (λ n : nat, a n /n))
:=
begin
  -- We extract a quasi-subadditivity constant for a
  rcases a_qsubadditive with ⟨ c : real, a_qsubadd ⟩,
  have c_nonneg: c ≥ 0, by
    calc c ≥ abs (a (1+1) - a 1 - a 1) : by exact a_qsubadd 1 1
       ... ≥ 0                         : by simp[abs_nonneg],   

  -- Then, c' = c + a 1 witnesses the boundedness from below:
  let c' : real := a 1 - c, 
  use c',
  -- We first show the corresponding estimate before normalisation
  have bdd_below': ∀ n : nat, a (n+1) ≥ (n+1) * c', from
  begin
    assume n : nat, 
    induction n with n ind_hyp,
    /- induction start n = 0 -/
    calc a (0 + 1) = a 1                      : by linarith
               ... ≥ 1 * (a 1 - c)            : by linarith
               ... = (↑0+1) * (a (0+1) - c)   : by norm_num,   
    /- induction step: n -> n + 1-/
    calc a (n.succ + 1) ≥ a (n+1) + a 1 - c   : by exact (qsubadd_without_abs a c a_qsubadd).1 (n+1) 1
                 ... ≥ (↑n+1) * c' + a 1 - c  : by linarith[ind_hyp]
                 ... = (↑n.succ + 1) * c'     : by {field_simp[c'], linarith},   
  end,     
  -- The actual estimate:
  show ∀ n : nat, n > 0 → a n/n ≥ c', from
  begin
    assume n : nat,
    assume n_pos: n > 0,
    have n_geq_1: n ≥ 1, by exact nat.succ_le_iff.mpr n_pos,

    -- rewriting n as k + 1
    let k : nat := n-1,
    have n_via_k : n = k + 1, by exact (nat.sub_eq_iff_eq_add n_geq_1).mp rfl,
    have k1_geq_0: 0 ≤ (k :real) + 1, by {norm_cast, simp[n_via_k, n_pos]},
    have k1_neq_0: (k: real) + 1 ≠ 0, by {norm_cast, linarith},

    -- finishing off the estimate
    calc a n / n = a (k + 1)/(k + 1)  : by {congr' 2, norm_cast, apply n_via_k}
             ... ≥ (k+1) * c' / (k+1) : by {apply div_le_div_of_le, apply k1_geq_0, 
                                            apply bdd_below' k} 
             ... = c'                 : by field_simp[k1_neq_0,mul_comm],
  end
end       

theorem quasi_fekete_estimate_leq
        (a : nat → real)
        (c : real)
        (a_qsubadd: ∀ n m : nat, 
                    abs( a (n+m) - a n - a m) ≤ c)
        (k : nat)  
        (k_pos: k > 0)              
      : is_convergent (λ n : nat, a n / n )
      ∧ (↑k * lim filter.at_top (λ n : nat, a n /n ) - a k ≤ c)
:=
begin
  /- The proof reduces the quasi-subadditive case to the 
     subadditive case and the usual Fekete lemma by means 
     of a helper sequence. -/
  have a_qsubadditive : is_quasi_subadditive a,
       by {use c, apply a_qsubadd},   
  have an_bddbelow: is_boundedbelow (λ n : nat, a n /n), 
       by exact qsubadd_bddbelow a a_qsubadditive,   
  have c_nonneg: c ≥ 0, 
    calc c ≥ abs (a (1+1) - a 1 - a 1) : by exact a_qsubadd 1 1
       ... ≥ 0                         : by simp[abs_nonneg],   

  -- We consider a helper sequence
  let v : nat → real := (λ n : nat, a n + c),

  -- This helper sequence is bounded below and subadditive
  have v_bddbelow: is_boundedbelow (λ n : nat, v n/ n), from
  begin
    -- We extract a lower bound for a n/n
    rcases an_bddbelow with ⟨ c' : real, an_bddb ⟩,
    -- Then c' witnesses that v n / n is bounded below
    use c',

    assume n : nat, 
    assume n_pos: n > 0,
    have n_geq_1: n ≥ 1, by exact nat.succ_le_iff.mpr n_pos,
    have n_nonneg: 0 ≤ (n :real), by simp,

    have an_geq_c': a n/n ≥ c', by field_simp[an_bddb, n_pos],
    have cn_geq_0: c/n ≥ 0, by exact div_nonneg c_nonneg n_nonneg,

    -- The actual lower bound estimate
    calc v n / n = (a n + c)/n    : by refl
             ... = a n / n + c /n : by field_simp
             ... ≥ c' + c / n     : by {apply add_le_add an_geq_c', refl}
             ... ≥ c'             : by {field_simp[add_le_add], apply cn_geq_0},
  end,

  have v_subadd: is_subadditive v, from
  begin
    assume n : nat,
    assume m : nat,
    show v (n+m) ≤ v n+ v m,    
      by calc v (n+m) = a (n+m) + c         : by refl
                  ... ≤ (a n + a m + c) + c : by {simp only [add_le_add_iff_right], 
                                                  apply (qsubadd_without_abs a c a_qsubadd).2 n m}
                  ... = a n + c + a m + c   : by linarith
                  ... = v n + a m + c       : by congr
                  ... = v n + v m           : by exact add_assoc (v n) (a m) c,
  end,

  -- Thus, we can apply the usual Fekete lemma to the helper sequence
  have conv_v : is_convergent (λ n : nat, v n / n),
       by exact (fekete v v_subadd v_bddbelow).1,
  have lim_v_est : ∀ k : nat, k > 0 → v k ≥ k * lim filter.at_top(λ n:  nat, v n/n),
       by exact (fekete v v_subadd v_bddbelow).2,

  -- We now translate the results back to the original sequence a:
  have part_1: is_convergent (λ n : nat, a n / n)
             ∧ (lim filter.at_top (λ n : nat, a n /n) 
              = lim filter.at_top (λ n : nat, v n /n)), from
  begin
    -- We write (a n / n)_n as the difference of two convergent sequences
    have an_via_v: (λ n : nat, a n / n) = (λ n : nat, v n / n - c / n), from
      begin
        funext, 
        calc a n / n = (a n + c - c) / n     : by simp
                 ... = (a n + c) / n - c / n : by exact sub_div (a n + c) c ↑n
                 ... = v n / n - c / n       : by simp,
      end,

    have conv_c_over_n : is_convergent (λ n : nat, c / n),
         by exact (lim_1_over_n c).2.1,
    have lim_c_over_n: lim filter.at_top (λ n : nat, c / n) = 0,
         by exact (lim_1_over_n c).2.2,  

    have subpart_1 : is_convergent (λ n : nat, a n / n),
         by {rw an_via_v, exact (lim_sub _ _ conv_v conv_c_over_n).1},     

    have subpart_2 : lim filter.at_top (λ n : nat, a n /n) 
                   = lim filter.at_top (λ n : nat, v n / n),
         by {rw an_via_v, simp[lim_c_over_n, lim_sub _ _ conv_v conv_c_over_n]},

    show _, by exact ⟨subpart_1, subpart_2⟩,
  end,

  have part_2: ↑k * lim filter.at_top (λ n : nat, a n /n ) - a k ≤ c, from
  begin
    calc ↑k * lim filter.at_top (λ n : nat, a n / n) - a k
         = ↑k * lim filter.at_top (λ n : nat, v n / n) - a k
         : by simp[part_1]
     ... ≤ v k - a k
         : by exact sub_le_sub_right (lim_v_est k k_pos) (a k)
     ... = c
         : by simp[v], 
  end,
  
  show _, by exact ⟨part_1.1, part_2⟩,
end

/- We state and prove the quasi-version of Fekete's lemma, 
both the convergence claim and the limit estimate.
-/

theorem quasi_fekete
        (a : nat → real)
        (a_qsubadditive: is_quasi_subadditive a)
      : is_convergent (λ n :nat, a n/n)
:=
begin 
  -- We extract a quasi-subadditivity constant for a ...
  rcases a_qsubadditive with ⟨ c : real, a_qsubadd ⟩,
  -- ... and apply the quantitative version of the Quasi-Fekete lemma.
  have one_pos: 1 > 0, by simp,
  show _,
       by exact (quasi_fekete_estimate_leq a c a_qsubadd 1 one_pos).1,
end                        


theorem quasi_fekete_estimate
        (a : nat → real)
        (c : real)
        (a_qsubadditive: ∀ n m : nat, 
                         abs( a (n+m) - a n - a m) ≤ c)
        (k : nat)
        (k_pos: k > 0)                
      : abs( ↑k * lim filter.at_top (λ n : nat, a n /n ) - a k  : real) ≤ c
:=
begin
  -- We apply quasi_fekete_estimate_leq to both a and -a
  have estimate_plus: ↑k * lim filter.at_top (λ n : nat, a n /n ) - a k ≤ c,
       by exact (quasi_fekete_estimate_leq a c a_qsubadditive k k_pos).2,
  have a_conv: is_convergent (λ n : nat, a n / n),
       by exact (quasi_fekete_estimate_leq a c a_qsubadditive k k_pos).1,

  have estimate_minus: - c ≤ ↑k * lim filter.at_top (λ n : nat, a n /n ) - a k, from
  begin
    let b : nat → real := (λ n : nat, - a n),
    have b_qsubadditive: ∀ n m : nat, 
                       abs( b (n+m) - b n - b m) ≤ c, from
         begin
           assume n m : nat,
           calc abs (b(n+m) - b n - b m) 
                 = abs( -a(n+m) - (- a n) - (- a m)) : by refl
             ... = abs( -(a (n+m) - a n -a m))       : by {congr, linarith}
             ... = abs( a(n+m) - a n - a m)          : by exact abs_neg (a (n + m) - a n - a m)
             ... ≤ c                                 : by exact a_qsubadditive n m, 
         end,    

    -- Then b satisfies the Fekete estimate:
    have est_b: ↑k * lim filter.at_top (λ n : nat, b n /n) - b k ≤ c,
         by exact (quasi_fekete_estimate_leq b c b_qsubadditive k k_pos).2,

    -- We can express the limit of (b n /n) in terms of a:
    have lim_b_via_a: lim filter.at_top (λ n : nat, b n / n) 
                  = - lim filter.at_top (λ n : nat, a n / n), by
      calc lim filter.at_top (λ n : nat, b n / n)
            = lim filter.at_top (λ n : nat, -a n / n) 
            : by refl
        ... = lim filter.at_top (λ n : nat, -1 * (a n / n)) 
            : by {congr' 1, funext, field_simp}
        ... = - 1 * lim filter.at_top (λ n : nat, a n / n) 
            : by exact lim_scalmul _ (-1) a_conv
        ... = - lim filter.at_top (λ n : nat, a n / n)
            : by linarith,

    -- Thus, we obtain an estimate for the limit of (a n /n):
    calc - c ≤ - (↑k * lim filter.at_top (λ n : nat, b n /n) - b k) 
             : by exact neg_le_neg est_b
         ... = - ↑k * lim filter.at_top (λ n : nat, b n /n) + b k 
             : by linarith
         ... = ↑k * lim filter.at_top (λ n : nat, a n / n) - a k
             : by {simp only[neg_mul, mul_neg, lim_b_via_a, neg_neg],ring_nf},  
  end,

  -- Combining the positive and the negative estimate gives the estimate 
  -- for the absolute value.
  show _, by {apply abs_le.mpr, 
              exact ⟨estimate_minus, estimate_plus⟩},
end

