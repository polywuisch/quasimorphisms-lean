/-
Copyright (c) 2022 Clara Löh. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Author: Clara Löh.
-/

import data.real.basic
import topology.basic
import topology.instances.real
import order.filter
import analysis.specific_limits.basic

/-!

# Basics on limits of sequences of real numbers

We collect selected basic properties of limits of sequences 
of real numbers. 

This is just a convenient wrapper around mathlib functionality.

-/

def is_convergent
    (a : nat → real)
:= ∃ c : real, filter.tendsto a filter.at_top (nhds c)

/- Converting between lim and tendsto, using tools from topology.separation -/
lemma tendsto_lim 
      (a : nat → real)
      (b : real)
      (a_tto_b : filter.tendsto a filter.at_top (nhds b))
    : lim filter.at_top a = b
:= filter.tendsto.lim_eq a_tto_b -- from topology.separation      

lemma lim_tendsto
      (a : nat → real)
      (b : real)
      (conv_a : is_convergent a)
      (a_lim_b : lim filter.at_top a = b)
    : filter.tendsto a filter.at_top (nhds b)
:= (filter.lim_eq_iff conv_a).mp a_lim_b


/-
## Some basics on the sequence `(1/n)_n`
-/

/- lim_n 1/n = 0 (Archimedes!) -/
lemma lim_1_over_n 
      (c : real)
    : (filter.tendsto (λ n : nat, c/(n : real)) filter.at_top (nhds 0))
    ∧ (is_convergent (λ n : nat, c/(n : real)))
    ∧ (lim filter.at_top (λ n : nat, c/(n : real)) = 0)
:=
begin
  have part_1 : filter.tendsto (λ n : nat, c/(n: real)) filter.at_top (nhds 0),
       by simp[tendsto_const_div_at_top_nhds_0_nat],
       
  have part_2: is_convergent (λ n : nat, c/(n :real)),
       by {use 0, apply part_1}, 

  have part_3: lim filter.at_top (λ n : nat, c/(n: real)) = 0,
       by exact tendsto_lim _ 0 part_1,

  show _, by exact ⟨part_1, part_2, part_3⟩,
end     

/- Showing that a real number is zero via Archimedes. -/
lemma zero_via_1_over_n
      (x : real)
      (c : real)
      (x_le_c_over_n: ∀ n : nat, 1 ≤ n → abs x ≤ c / ↑n)
    : x = 0
:=
begin
  -- we first show that the absolute value of x is zero
  have abs_x_eq_0 : abs x = 0, from
  begin 
    have absx_geq_0 : abs x ≥ 0,
         by simp, 
  
    have absx_leq_0: abs x ≤ 0, from
    begin
      -- x is dominated by lim_n c/n = 0 and so x ≤ 0
      have tto_0: filter.tendsto (λ n: nat, c/↑n) filter.at_top (nhds 0),
           by exact (lim_1_over_n c).1,
      have x_le_c_over_n': ∀ᶠ (n : nat) in filter.at_top, abs x ≤ c / ↑n,
           by {simp only [filter.eventually_at_top], use 1, exact x_le_c_over_n}, 
      show abs x ≤ 0,
           by simp[ge_of_tendsto tto_0 x_le_c_over_n']
    end,
    show abs x = 0,
         by {simp only[abs_eq_zero], exact abs_nonpos_iff.mp absx_leq_0},
  end,
  -- thus, x must be zero as well
  show x = 0,
       by exact abs_eq_zero.mp abs_x_eq_0,
end     

/-
## Basic arithmetic on limits
-/

lemma tendsto_div_at_top_iff_nat 
      {α : Type*}
      {f : ℕ → α} 
      {l : filter α} 
      (k : ℕ) 
      (k_pos: 0 < k)
    : filter.tendsto (λn, f (n/k)) filter.at_top l 
    ↔ filter.tendsto f filter.at_top l 
:=
begin
  show _, 
       by {rw [← filter.tendsto_map'_iff, filter.map_div_at_top_eq_nat], 
           exact k_pos},
end

lemma tendsto_mul_at_top 
      (n : ℕ) 
      (n_pos : 0 < n) 
    : filter.tendsto (λ m, m * n) filter.at_top filter.at_top 
:=
begin
  -- we use standard filter properties for monotone maps
  have mono_mul : monotone (λ m , m * n),
       by {unfold monotone, simp[mul_mono_nonneg, n_pos]},

  have large_mul : ∀ b : ℕ, ∃ m, b ≤ m * n, from 
  begin 
    assume b : ℕ, 
    use b,
    have n_geq_1 : 1 ≤ n, by linarith[n_pos],
    calc b ≤ b * 1 : by simp 
       ... ≤ b * n : by exact mul_le_mul_left' n_geq_1 b,
  end,

  show _,
       by exact filter.tendsto_at_top_at_top_of_monotone mono_mul large_mul,
end

lemma lim_of_argmul
      (a : nat → real)
      (n : nat)
      (n_non0: n ≠ 0)
      (conv_a: is_convergent a) 
    : (is_convergent (λ m : nat, a (m*n)))
    ∧ (lim filter.at_top (λ m : nat, a (m * n)) = lim filter.at_top a)
:=
begin
  -- the reparametrised sequence
  let an : nat → real := (λ m : nat, a (m*n)),

  -- the limit of a
  rcases conv_a with ⟨ b, a_tto_b ⟩,
  have a_lim_b : lim filter.at_top a = b,
       by {apply tendsto_lim _ b a_tto_b},

  -- the tendsto-version of the claim
  have tto_version: filter.tendsto an filter.at_top (nhds b), from
  begin
    have n_pos: 0 < n, by exact zero_lt_iff.mpr n_non0,

    show _, 
         by exact filter.tendsto.comp a_tto_b (tendsto_mul_at_top n n_pos), 
  end,

  have part_1 : is_convergent (λ m : nat, a (m*n)), 
       by {use b, exact tto_version},

  have part_2 : lim filter.at_top an = lim filter.at_top a,
       by simp[tendsto_lim an b tto_version, a_lim_b],

  show _, by exact ⟨part_1, part_2⟩,
end       

lemma lim_scalmul
      (a : nat → real)
      (c : real)
      (conv_a: is_convergent a)
    : lim filter.at_top (λ m : nat, c * a m) = c * lim filter.at_top a 
:=
begin
  -- extracting a limit of a
  rcases conv_a with ⟨b : real, a_tto_b⟩,
  have a_lim_b : lim filter.at_top a = b,
       by exact tendsto_lim a b a_tto_b,

  -- the lemma as tendsto-version
  have tto_version: filter.tendsto (λ m: nat, c * (a m)) filter.at_top (nhds (c * b)),
       by exact filter.tendsto.const_mul c a_tto_b,
  -- converting the tendsto-version into the lim-version.
  calc  lim filter.at_top (λ m : nat, c * a m) 
      = c * b
      : by exact tendsto_lim _ _ tto_version 
  ... = c * lim filter.at_top a
      : by rw[a_lim_b], 
end

lemma lim_sub
      (a_1 a_2 : nat → real)
      (conv_a1: is_convergent a_1)
      (conv_a2: is_convergent a_2)
    : (is_convergent (λ n : nat, a_1 n - a_2 n) )
    ∧ (  lim filter.at_top (λ n : nat, a_1 n - a_2 n) 
       = lim filter.at_top a_1 - lim filter.at_top a_2)
:=
begin
  -- extracting limits of a_1 and a_2
  rcases conv_a1 with ⟨ b_1 : real, a1_tto_b1 ⟩,
  rcases conv_a2 with ⟨ b_2 : real, a2_tto_b2 ⟩,
  have a1_lim_b1 : lim filter.at_top a_1 = b_1, by exact tendsto_lim a_1 b_1 a1_tto_b1,
  have a2_lim_b2 : lim filter.at_top a_2 = b_2, by exact tendsto_lim a_2 b_2 a2_tto_b2,

  -- the lemma as tendsto-version
  have tto_version: filter.tendsto (λ n : nat, a_1 n - a_2 n) filter.at_top (nhds (b_1 - b_2)),
       by exact filter.tendsto.sub a1_tto_b1 a2_tto_b2,

  have part_1 : is_convergent (λ n : nat, a_1 n - a_2 n), 
       by {use (b_1 - b_2), exact tto_version},

  -- converting the tendsto-version into the lim-version.
  have part_2 :  lim filter.at_top (λ n : nat, a_1 n - a_2 n) 
               = lim filter.at_top a_1 - lim filter.at_top a_2,
  calc lim filter.at_top (λ n : nat, a_1 n - a_2 n)
       = b_1 - b_2 
       : by exact tendsto_lim _ _ tto_version
   ... = lim filter.at_top a_1 - lim filter.at_top a_2 
       : by simp[a1_lim_b1,a2_lim_b2],    

  show _, by exact ⟨part_1, part_2⟩,
end


/- Convergence of an almost constant sequence.-/
lemma lim_eventually_const
      (a : ℕ → real)
      (c : real)
      (a_ev_const: ∀ n : nat, 1 ≤ n → a n = c)
    : lim filter.at_top a = c
:=
begin
  -- we consider the shifted sequence
  have a'_const_c : ∀ n : nat, a (n + 1) = c, from
       begin
         assume n : nat, 
         have n_geq_1: n + 1 ≥ 1, by simp,
         show a (n+1) = c,
              by simp[n_geq_1, a_ev_const],
       end,
  have a'_tto_c : filter.tendsto (λ n, a (n+1)) filter.at_top (nhds c),
       by {simp only[a'_const_c], exact tendsto_const_nhds},
  have a_tto_c : filter.tendsto a filter.at_top (nhds c),
       by {apply (filter.tendsto_add_at_top_iff_nat 1).mp, exact a'_tto_c},
  show lim filter.at_top a = c,
       by exact tendsto_lim a c a_tto_c,
end


