/-
Copyright (c) 2022 Clara Löh. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Author: Clara Löh.
-/

import algebra.group
import group_theory.free_group
import data.real.basic
import topology.basic
import topology.instances.real
import algebra.module.submodule.basic
import algebra.module.pi

import lim_basics
import fekete

/-! 
# Quasimorphisms

This file defines the notion of quasimorphism 
and collects basic proerties of quasimorphisms.
-/


/-
## Definition of quasimorphisms
-/

/- A quasimorphism is a map from a group to ℝ 
whose mutliplicative defect is bounded.
-/

/- Measuring the (multiplicative) defect of the given 
function on the arguments. -/
def mult_defect 
    (G : Type*) [group G] 
    (f : G → real) 
    (x : G) (y : G) 
:=  f (x * y) - f x - f y 
   
/- Testing whether on the given group the given function 
is a quasi-morphism or not. -/   
def is_quasimorphism 
    (G : Type*) [group G]
    (f : G → real) 
:= ∃ c : real, ∀ x : G, ∀ y : G, 
               abs (mult_defect G f x y) ≤ c 

structure quasimorphism 
          (G : Type*) [group G] 
:= (f : G → real)
   (map_qm : is_quasimorphism G f)

/- The set of all quasimorphisms on a given group. -/
def QM (G : Type*) [group G] 
:= { f : G → real | is_quasimorphism G f }

def has_finite_distance 
    (G : Type*)
    (f : G → real)
    (g : G → real) 
:= ∃ c : real, ∀ x : G, abs (f x - g x) ≤ c

/- Iterated triangle inequality 
(there should be a better way of doing this) -/
lemma abs_add4
      (a b c d: real)
    : abs (a + b + c + d) ≤ abs a + abs b + abs c + abs d
:=
begin
  calc 
    abs (a + b + c + d) ≤ abs (a + b + c) + abs d       : by simp[abs_add]
                   ...  ≤ abs (a + b) + abs c + abs d   : by simp[abs_add]     
                   ...  ≤ abs a + abs b + abs c + abs d : by simp[abs_add],
end 

/-
## Basic inheritance properties of quasimorphisms
-/

/- If a map has finite distance to a quasimorphism, then 
it is a quasimorphism. -/  
lemma finite_distance_to_qm_is_qm 
      (G : Type*) [group G]
      (f : G → real) 
      (g : G → real) 
      (g_is_qm: is_quasimorphism G g)
      (fg_finite_distance: has_finite_distance G f g) 
    : is_quasimorphism G f
:= 
begin
 rcases g_is_qm with ⟨ cg : real, mult_def_g ⟩,
 rcases fg_finite_distance with ⟨ cfg : real, dist_fg ⟩, 
 /-obtain ⟨ cg : real, mult_def_g ⟩ : is_quasimorphism G g, -/
 let c : real := 3 * cfg + cg,
 have f_is_qm: ∀ x : G, ∀ y : G, abs (mult_defect G f x y) ≤ c, from
 begin
     assume x : G,
     assume y : G,
     calc     abs (mult_defect G f x y)
          =   abs ( mult_defect G f x y
                  + mult_defect G g x y - mult_defect G g x y )
            : by {congr, linarith} 
      ... =  abs (  (f (x * y) - g (x * y))
                  + (- f x + g x)
                  + (- f y + g y)
                  + mult_defect G g x y )
            : by {unfold mult_defect, congr, linarith}  
      ... ≤   abs ( f (x * y) - g (x * y) )
            + abs (-f x + g x)
            + abs (-f y + g y)
            + abs (mult_defect G g x y)
            : by simp[abs_add4]
      ... ≤   cfg 
            + abs (-f x + g x)
            + abs (-f y + g y)
            + abs (mult_defect G g x y)
            : by simp[dist_fg]
      ... ≤   cfg 
            + cfg
            + abs (- f y + g y)
            + abs (mult_defect G g x y)
            : by {rw ←abs_neg, 
                  simp only [add_le_add_iff_left, add_le_add_iff_right, neg_add_rev, neg_neg], 
                  rw add_comm, 
                  apply dist_fg}
      ... ≤   cfg 
            + cfg
            + cfg
            + abs (mult_defect G g x y)
            : by {rw ←abs_neg, 
                  simp only [add_le_add_iff_left, add_le_add_iff_right, neg_add_rev, neg_neg], 
                  rw add_comm, 
                  apply dist_fg}
      ... ≤   cfg 
            + cfg
            + cfg
            + cg
            : by simp[add_le_add,mult_def_g]
      ... = 3 * cfg + cg
            : by linarith
      ... = c
            : by refl,
 end, 
 show is_quasimorphism G f, 
      by {unfold is_quasimorphism, use c, exact f_is_qm},
end

/- The pointwise sum of two quasimorphisms is a quasimorphism. -/
lemma sum_of_qm_is_qm
      (G : Type*) [group G]
      (f g: G → real)
      (f_is_qm: is_quasimorphism G f)
      (g_is_qm: is_quasimorphism G g)
    : is_quasimorphism G (λ x, f x + g x) 
:=
begin
  /- The pointwise sum function -/
  let fg : G → real := λ x, f x + g x,
  /- Extracting defects cf and cg from f and g, respectively -/
  rcases f_is_qm with ⟨ cf : real, mult_def_f ⟩,
  rcases g_is_qm with ⟨ cg : real, mult_def_g ⟩,
  /- The expected defect for the sum function -/
  let c : real := cf + cg,
  /- Establishing the defect estimate for the sum function -/
  have mult_def_fg: ∀ x : G, ∀ y : G, abs (mult_defect G fg x y) ≤ c, from
  begin
    assume x : G,
    assume y : G,
    /- Additivity of defect -/
    have add_mult_defect: mult_defect G fg x y 
                       = mult_defect G f x y + mult_defect G g x y, from
      begin
      calc     mult_defect G fg x y
           =   fg (x * y) - fg x - fg y 
             : by {unfold mult_defect}
       ... =   f (x * y) + g (x * y) - f x - g x - f y - g y
             : by simp[sub_add_eq_sub_sub]
       ... = mult_defect G f x y + mult_defect G g x y
             : by {unfold mult_defect, linarith},      
      end,       
    /- Taking the absolute value of the additivity of defect -/  
    calc     abs (mult_defect G fg x y)
         ≤   abs (mult_defect G f x y) + abs (mult_defect G g x y)
           : by simp[add_mult_defect, abs_add]
     ... ≤   cf + cg 
           : by simp[mult_def_f, mult_def_g, add_le_add]  
     ... ≤ c 
           : by simp,   
  end,
  show is_quasimorphism G fg, 
       by {unfold is_quasimorphism, use c, exact mult_def_fg},
end

/- The scalar multiple of a  quasimorphism is a quasimorphism. -/
lemma scalarmult_of_qm_is_qm
      (G : Type*) [group G]
      (f : G → real)
      (a : real)
      (f_is_qm: is_quasimorphism G f)
    : is_quasimorphism G (λ x, a * f x ) 
:=
begin
  /- The scalar multiple -/
  let fa : G → real := λ x, a * f x,
  /- Extracting the defect cf from f -/
  rcases f_is_qm with ⟨ cf : real, mult_def_f ⟩,
  /- The expected defect for the sum function -/
  let c : real := abs a * cf,
  /- Establishing the defect estimate for the scalar multiple -/
  have mult_def_fa: ∀ x : G, ∀ y : G, abs (mult_defect G fa x y) ≤ c, from
  begin
    assume x : G,
    assume y : G,
    calc     abs (mult_defect G fa x y)
         =   abs (fa (x*y) - fa x - fa y)
           : by unfold mult_defect
     ... =   abs ( a * f (x*y) - a * f x - a * f y )
           : by congr
     ... =   abs ( a * mult_defect G f x y )
           : by {unfold mult_defect, congr, linarith}
     ... =   abs a * abs (mult_defect G f x y)
           : by exact abs_mul a (mult_defect G f x y)
     ... ≤   abs a * cf 
           : by simp[mul_le_mul,mult_def_f,abs_nonneg]
     ... = c
           : by refl,    
  end,
  show is_quasimorphism G fa, 
       by {unfold is_quasimorphism, use c, exact mult_def_fa},
end

/- The composition of a group homomorphism with a quasimorphism
is a quasimorphism. -/
lemma comp_hom_qm_is_qm
      (G : Type*) [group G]
      (H : Type*) [group H]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
      (h : mul_hom H G)
    : is_quasimorphism H (f ∘ h)  
:=
begin
  /- The composition -/
  let fh : H → real := f ∘ h,
  /- Extracting the defect c from f -/
  rcases f_is_qm with ⟨ c : real, mult_def_f ⟩,
  /- Establishing the defect estimate for the composition -/
  have mult_def_fh: ∀ x : H, ∀ y : H, abs (mult_defect H fh x y) ≤ c, from
  begin
    assume x : H,
    assume y : H,
    calc     abs (mult_defect H fh x y)
         =   abs (fh (x * y) - fh x - fh y)
           : by unfold mult_defect
     ... =   abs ( f (h (x * y)) - f (h x) - f(h y) ) 
           : by refl
     ... =   abs ( f ((h x) * (h y)) - f (h x) - f(h y) )
           : by simp      
     ... ≤ c
           : by exact (mult_def_f (h x) (h y)),      
  end,
  show is_quasimorphism H fh,
       by {unfold is_quasimorphism, use c, exact mult_def_fh}, 
end

/- 
## Homogeneous quasimorphisms 
-/

/- A quasimorphism is homogeneous if it is linear on cyclic subgroups;
it suffices to consider exponents in ℕ as will be shown below (`homogeneous_qm_on_inv`). -/
def is_homogeneous 
    (G : Type*) [group G]
    (f : G → real) 
:= ∀ x : G, ∀ n : nat, f(x^n) = n * f x

def is_homogeneous_qm 
    (G : Type*) [group G]
    (f : G → real) 
:= (is_quasimorphism G f) 
 ∧ (is_homogeneous G f)

/- Homomorphisms to the (additive) reals are quasimorphism. -/

/- In order to avoid confusion about multiplicative/additive 
notation, we first use an ad-hoc definition of the homomorphism 
property and then provide a wrapper for monoid homomorphisms.-/

def is_hom_to_reals
    (G : Type*) [group G]
    (f : G → real)
:= ∀ x : G, ∀ y : G, f(x * y) = f x + f y  

lemma homtoreals_is_qm'
      (G : Type*) [group G]
      (f : G → real)
      (f_is_hom: is_hom_to_reals G f)
    : is_quasimorphism G f
:=
begin
  let c : real := 0,
  have mult_def_f: ∀ x : G, ∀ y : G, abs (mult_defect G f x y) ≤ c, from
  begin
    assume x : G, 
    assume y : G,
    calc      abs (mult_defect G f x y)
          =   abs (f (x * y) - f x - f y)
            : by unfold mult_defect
      ... =   abs (f x + f y - f x - f y)
            : by {congr, apply f_is_hom}      
      ... ≤ c
            : by simp,  
  end, 
  show is_quasimorphism G f,
       by {unfold is_quasimorphism, use c, exact mult_def_f},
end    

/- multiplicative real denotes the monoid of real numbers 
with respect to addition, but written multiplicatively. -/
lemma grouphom_is_hom_to_reals 
      (G : Type*) [group G]
      (f : monoid_hom G (multiplicative real))
    : is_hom_to_reals G (multiplicative.to_add ∘ f.to_fun)
:=
begin
  /- We convert the monoid_hom condition and multiplicative notation 
  into the additive notation of is_hom_to_reals. -/
  let f' : G → real := multiplicative.to_add ∘ f.to_fun,   
  have f_is_hom: is_hom_to_reals G f', from
  begin
    assume x : G,
    assume y : G,
    calc f' (x * y) = (multiplicative.to_add ∘ f.to_fun) (x * y)
                    : by refl
                ... = multiplicative.to_add (f.to_fun x * f.to_fun y)
                    : by simp[f.map_mul' x y]
                ... = f.to_fun x + f.to_fun y
                    : by apply to_add_mul 
                ... = f' x + f' y
                    : by refl, 
  end,
  show is_hom_to_reals G f',
       by simp[f_is_hom],
end

lemma homtoreals_is_qm 
      (G : Type*) [group G]
      (f : monoid_hom G (multiplicative real))
    : is_quasimorphism G (multiplicative.to_add ∘ f.to_fun)    
:= 
begin
  let f' : G → real := multiplicative.to_add ∘ f.to_fun,   
  /- We convert the monoid_hom condition and multiplicative notation 
  into the additive notation of is_hom_to_reals and then apply 
  the lemma on homomorphisms to reals. -/
  have f_is_hom: is_hom_to_reals G f', 
       by simp[grouphom_is_hom_to_reals G f],
  show is_quasimorphism G f',
       by simp[f_is_hom, homtoreals_is_qm' G f'],
end

/- Homomorphisms to ℝ are homogeneous -/
lemma homtoreals_is_homogeneous_qm' 
      (G : Type*) [group G]
      (f : G → real)
      (f_is_hom : is_hom_to_reals G f)
    : is_homogeneous_qm G f
:=
begin
  /- We already know that f is a quasimorphism. -/
  have is_qm_f: is_quasimorphism G f,
       by simp[f_is_hom,homtoreals_is_qm' G f],
  /- Moreover, f is homogeneous: -/
  have is_homg_f: is_homogeneous G f, from 
  begin    
    assume x : G, 
    assume n : nat,
    show f (x^n) = n * f x, from
    begin
      induction n with n ind_hyp,
      /- induction start: n = 0 -/
      begin
        calc  
          f (x^0) = f 1                : by simp
              ... = (f 1 + f 1) - f 1  : by linarith
              ... = f (1 * 1) - f 1    : by rw f_is_hom
              ... = f 1 - f 1          : by simp        
              ... = 0                  : by linarith
              ... = ↑0 * f x           : by simp     
      end, 
      /- induction step: n -> n + 1 -/
      begin 
        calc
          f (x^(n.succ)) = f(x^n * x)       : by {congr, exact pow_succ' x n}
                     ... = f(x^n) + f x     : by rw f_is_hom
                     ... = n * f x + f x    : by simp[ind_hyp]
                     ... = (n.succ) * f x   : by {norm_num,ring_nf},
      end,
    end,
  end,
  /- Therefore, f is a homoegeneous quasimorphism. -/
  show is_homogeneous_qm G f,
       by {unfold is_homogeneous_qm, exact ⟨is_qm_f, is_homg_f⟩},
end 

lemma homtoreals_is_homogeneous_qm 
      (G : Type*) [group G]
      (f : monoid_hom G (multiplicative real))
    : is_homogeneous_qm G (multiplicative.to_add ∘ f.to_fun)    
:= 
begin
  let f' : G → real := multiplicative.to_add ∘ f.to_fun,   
  /- We convert the monoid_hom condition and multiplicative notation 
  into the additive notation of is_hom_to_reals and then apply 
  the lemma on homomorphisms to reals. -/
  have f_is_hom: is_hom_to_reals G f', 
       by simp[grouphom_is_hom_to_reals G f],
  show is_homogeneous_qm G f',
       by simp[f_is_hom, homtoreals_is_homogeneous_qm' G f'],
end

/-
## Basic examples
-/

/- The zero map is a quasimorphism. -/
lemma zero_is_qm 
      (G : Type*) [group G]
    : is_quasimorphism G (λ _, 0)
:=
begin
  let f : G → real := (λ _, 0),
  /- The zero map is a group homomorphism ... -/
  have f_is_hom : is_hom_to_reals G f, 
       by {unfold is_hom_to_reals, simp},
  /- ... and thus a quasimorphism. -/
  show is_quasimorphism G f, 
       by simp[f_is_hom, homtoreals_is_qm' G f]
end

/- Bounded maps are quasimorphisms. -/
def is_bounded
    (G : Type*)
    (f : G → real)
:= ∃ c : real, ∀ x : G, abs (f x) ≤ c     

lemma bounded_map_is_qm
      (G : Type*) [group G]
      (f : G → real)
      (f_is_bounded: is_bounded G f)
    : is_quasimorphism G f
:=
begin
  /- We extract a bound for f. -/
  rcases f_is_bounded with ⟨ c : real, f_bdd ⟩,
  let g : G → real := (λ _, 0), 
  /- Bounded maps have finite distance from the zero map ... -/
  have f_findist_0 : has_finite_distance G f g, from
  begin
    use c,
    assume x : G,
    calc   abs (f x - g x) 
         = abs (f x)
         : by {congr, simp} /- g is the zero map -/
     ... ≤ c 
         : by simp[f_bdd],
  end, 
  /- ... and thus are quasimorphisms.-/
  show is_quasimorphism G f,
       by simp[f_findist_0, finite_distance_to_qm_is_qm G f g, zero_is_qm G]
end

/- Quasimorphisms are called trivial if they have finite distance 
from a homomorphism to the reals. -/
def is_trivial_qm
    (G : Type*) [group G]
    (f : G → real)
:= ∃ g : G → real, is_hom_to_reals G g 
                 ∧ has_finite_distance G f g

/- Trivial quasimorphisms indeed are quasimorphisms. -/
lemma trivial_qm_is_qm
      (G : Type*) [group G]
      (f : G → real)
      (f_is_trivial: is_trivial_qm G f)
    : is_quasimorphism G f
:=
begin
  /- We extract a homomorphism g that has finite distance to f. -/
  rcases f_is_trivial with ⟨ g : G → real, ⟨g_is_hom, findist_fg⟩ ⟩,
  /- In particular, g is a quasimorphism. -/
  have g_is_qm: is_quasimorphism G g, 
       by simp[g_is_hom, homtoreals_is_qm' G g], 
  show is_quasimorphism G f,
       by simp[g_is_qm,finite_distance_to_qm_is_qm G f g, findist_fg],
end


/-
## The set of quasimorphisms forms a vector space
-/

/- We first show that `QM G` is a subspace of the function space `G → ℝ`. 
-/
def QM_subspace_of_functions 
     (G :Type) [group G]
   : submodule ℝ (G → ℝ)
:= 
{ carrier   := QM G,
  add_mem'  := λ f g : G → ℝ, λ f_in_QM : f ∈ QM G, λ g_in_QM : g ∈ QM G, 
               sum_of_qm_is_qm G f g f_in_QM g_in_QM, 
  zero_mem' := zero_is_qm G, 
  smul_mem' := λ a : ℝ, λ f : G → ℝ, λ f_in_QM : f ∈ QM G, 
               scalarmult_of_qm_is_qm G f a f_in_QM 
}

/- We can thus use that subspaces are vector spaces.
-/
instance QM_is_vectorspace 
    (G : Type) [G_is_group: group G]
  : @module ℝ (QM G) 
            _ (submodule.add_comm_monoid (QM_subspace_of_functions G))
:= submodule.module (QM_subspace_of_functions G) 


/- 
## Homogenisation of quasimorphisms 
-/

/- The homogenisation construction turns a quasimorphism into 
a homogeneous quasimorphism that is only finite distance away. 
Homogenisation is defined through the average of the values on 
powers. -/

noncomputable
def homogenisation 
    (G : Type*) [group G]
    (f : G → real)
    (f_is_qm: is_quasimorphism G f)
:= λ x : G, lim filter.at_top (λ n : nat,  f(x^n)/n) 

/- We first establish convergence of these averages (and whence 
that the limit is an actual limit). This uses a version of Fekete's 
lemma.-/

lemma qm_quasi_subadditive_explicit 
      (G : Type*) [group G]
      (f : G → real)
      (c : real)
      (f_is_qm: ∀ x y : G, abs(mult_defect G f x y) ≤ c)
      (x : G)
    : ∀ n m : nat, abs (f(x^(n+m)) - f(x^n) - f(x^m)) ≤ c
:=
begin
    -- as f is a quasimorphism, f is subadditive on powers of x
    assume n : nat,
    assume m : nat, 
    calc abs (f(x^(n+m)) - f(x^n) - f(x^m))
            = abs (f (x^n * x^m) - f (x^n) - f(x^m)) 
            : by {congr, simp[pow_add]}
        ... ≤ c
            : by exact (f_is_qm (x^n) (x^m)),
end 

lemma qm_quasi_subadditive
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
      (x : G)
    : is_quasi_subadditive (λ n : nat, f(x^n))
:=
begin
  show ∃ c : real, ∀ n m : nat, 
                   abs ( f(x^(n+m)) - f(x^n) - f(x^m))  ≤ c, from
  begin
    -- extracting a defect for f
    rcases f_is_qm with ⟨ c : real, mult_def_f ⟩,
    -- then c witnesses that a is subadditive
    use c,
    exact qm_quasi_subadditive_explicit G f c mult_def_f x,
  end,
end

lemma qm_normalisedpowers_converge
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
      (x : G)
    : is_convergent (λ n : nat, f(x^n)/n)
:=
begin
  -- as f is a quasimorphism, f is subadditive on powers of x
  let a : nat → real := λ n, f (x^n),
  have a_quasi_subadd: is_quasi_subadditive a,
       by exact qm_quasi_subadditive G f f_is_qm x,
  -- thus, we can apply Fekete's lemma     
  show is_convergent (λ n : nat, f (x^n)/n),
       by exact quasi_fekete a a_quasi_subadd,
end


/- The homogenisation has finite distance to the original quasimorphism. -/
lemma homogenisation_finite_distance
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
    : has_finite_distance G (homogenisation G f f_is_qm) f
:=
begin
  -- the homogenisation of f
  let g : G → real := homogenisation G f f_is_qm,
  -- we extract a defect of f 
  rcases f_is_qm with ⟨ c : real, f_mult_def ⟩,
  -- then the homogenisation of f has distance at most c from f:
  have dist_gf : ∀ x : G, abs (g x - f x) ≤ c, from
  begin
    assume x : G,
    -- the relevant sequences
    let a  : nat → real := λ n, f (x^n),
    let na : nat → real := λ n, a n / n,
    have a_quasi_subadd: ∀ n m : nat, abs(a (n+m) - a n - a m) ≤ c,
         by exact qm_quasi_subadditive_explicit G f c f_mult_def x,
    -- the explicit version of the Fekete lemma gives the distance bound:    
    have lim_est: abs (lim filter.at_top na - f x) ≤ c, from
    begin
      have one_pos: 1 > 0, by simp,
      calc abs (lim filter.at_top na - f x)
           = abs (((1 : nat) :real) * lim filter.at_top (λ n : nat, a n/n) - f(x^1) )
           : by simp
       ... = abs (((1 : nat) : real) * lim filter.at_top (λ n : nat, a n/n) - a 1)
           : by refl
       ... ≤ c
           : by exact quasi_fekete_estimate a c a_quasi_subadd 1 one_pos, 
    end, 
    show abs (g x - f x) ≤ c, 
         by simp[g,lim_est,homogenisation],
  end,
  show has_finite_distance G g f,
       by {use c, simp[has_finite_distance, dist_gf]},
end

/- The homogenisation of a quasimorphism is a quasimorphism. -/
lemma homogensiation_is_qm
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
    : is_quasimorphism G (homogenisation G f f_is_qm)
:=
begin
  let g : G → real := homogenisation G f f_is_qm,
  show is_quasimorphism G g, 
       by  simp[f_is_qm, 
           homogenisation_finite_distance G f f_is_qm, 
           finite_distance_to_qm_is_qm G g f],
end

/- The homogenisation of a qm maps 1 to 0. -/
lemma homogenisation_on_one 
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
    : (homogenisation G f f_is_qm) 1 = 0
:=
begin
  -- this relies on lim_n 1/n = 0
  calc (homogenisation G f f_is_qm) 1 
     = lim filter.at_top (λ n : nat, f(1^n)/n)
     : by refl
 ... = lim filter.at_top (λ n : nat, f 1 /n)
     : by simp
 ... = 0
     : by exact (lim_1_over_n (f 1)).2.2,
end    


/- The homogenisation of a quasimorphism is homogeneous. -/
lemma homogenisation_is_homogeneous
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
    : is_homogeneous G (homogenisation G f f_is_qm)
:=
begin
  let g : G → real := homogenisation G f f_is_qm,
  assume x : G,
  assume n : nat,
  show g (x^n) = n * g x, from
  begin
    -- we have to treat the cases n = 0 and n > 0 differently
    induction n with k _,
    -- n = 0
    begin
      calc g(x ^ 0) = g 1      : by simp
                ... = 0        : by exact homogenisation_on_one G f f_is_qm
                ... = ↑0 * g x : by norm_num,
    end,
    -- n = k + 1 > 0
    begin
      let n : nat := k + 1,
      have n_non0 : (n : real) ≠ 0, by {norm_cast,simp},
      have n_non0' : (n : nat) ≠ 0, by simp,
      -- the sequence for g (x^n)
      let an : nat → real := (λ m, f((x^n)^m)/m),
      have g_xn : g(x^n) = lim filter.at_top an,
           by refl,
      -- we now rewrite an, using the sequence for g x
      let a : nat → real := (λ m, f(x^m)/m),
      have conv_a: is_convergent a,
           by {apply qm_normalisedpowers_converge G f f_is_qm},
      have rewrite_an : an  = λ m : nat, n * a (m * n), from
      begin
        funext,
        -- assume m : nat,
        calc an m = f((x^n)^m)/m             : by refl
              ... = f(x^(n*m)) * 1/m         : by simp[pow_mul] 
              ... = n * (f(x^(n*m))/n) * 1/m : by {congr, rw mul_div_cancel' _ n_non0}
              ... = n * (f(x^(n*m))/ (n*m))  : by field_simp
              ... = n * a (n * m)            : by norm_cast 
              ... = n * a (m * n)            : by rw mul_comm n m,
      end,
      -- this rewriting allows us to compute the limit of an
      have lim_an : lim filter.at_top an = n * lim filter.at_top a, from
      begin
        calc lim filter.at_top an 
             = lim filter.at_top (λ m : nat, n * a( m * n))
             : by congr' 1
         ... = n * lim filter.at_top (λ m : nat, a( m * n))
             : by {rw lim_scalmul (λ m : nat, a(m*n)) n _, 
                   exact (lim_of_argmul a n n_non0' conv_a).1}    
         ... = n * lim filter.at_top a
             : by {congr' 1, exact (lim_of_argmul a n n_non0' conv_a).2},
      end,
      -- finally, we can combine everything
      calc g (x^n) = lim filter.at_top an    : by refl
               ... = n * lim filter.at_top a : by simp[lim_an]
               ... = n * g x                 : by refl,
    end,
  end,  
end      

/- The homogenisation of a homogeneous quasimorphism 
   coincides with the original quasimorphism. In particular, 
   the homogenisation of the homogenisation is the same as 
   the homogenisation. -/
lemma homogenisation_of_homogeneous_qm
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
      (f_is_hmg: is_homogeneous G f)   
    : homogenisation G f f_is_qm = f
:=
begin
  let g : G → real := homogenisation G f f_is_qm,
  -- We prove pointwise equality of g and f
  have g_eq_f: ∀ x : G, g x = f x,
  begin 
    assume x : G,
    -- If f is homogeneous, then the average in the definition 
    -- of the homogenisation is (eventually) constant f x
    let a : nat → real := ( λ n, f(x^n)/n ),
    have a_const_fx : ∀ n : nat, n > 0 → a n = f x, from
    begin
      assume n : nat,
      assume n_pos : n > 0,
      have n_non0 : (n : real) ≠ 0, by { norm_cast, exact ne_of_gt n_pos },
      calc a n = f(x^n) / n     : by simp
           ... = n * f x / n    : by {congr, rw f_is_hmg}
           ... = n * (f x / n)  : by ring    
           ... = f x            : by exact mul_div_cancel' (f x) n_non0,
    end,
    -- Therefore, the limit is also f x
    have a_lim: lim filter.at_top a = f x,
         by simp[lim_eventually_const a (f x) a_const_fx],
    show g x = f x,
         by {simp only[g,homogenisation G f f_is_qm], unfold homogenisation,
             simp[a_lim]}, 
  end,
  -- Thus, by extensionality, g and f are equal.
  show g = f,
       by {apply funext, simp[g_eq_f]},
end       


/- Homogeneous quasimorphisms map inverses to their additive inverse. -/
lemma homogeneous_qm_on_inv
      (G : Type*) [group G]
      (f : G → real)
      (f_is_qm: is_quasimorphism G f)
      (f_is_hmg: is_homogeneous G f)
      (x : G)
    : f (x⁻¹) = - f x
:=
begin
  -- we extract a defect of f
  rcases f_is_qm with ⟨ c : real, mult_def_f ⟩,

  let y : G := x⁻¹,

  -- we show that |f(x) + f(x^-1)| is arbitrarily small ...
  have abs_sum_est : ∀ n : nat, n > 0 → 
       abs ( f x + f y ) ≤ (c + abs(f 1)) / n, from 
  begin
    assume n : nat, 
    assume n_pos : 0 < n,
    have n_non0 : n ≠ 0, by exact ne_of_gt n_pos,
    -- to keep the estimates simple, we first estimate the n-fold term
    have n_fold_est : abs ( (n : real) * (f x) + n * (f y) ) ≤ c + abs (f 1), from
    begin
      calc  abs ( (n : real) * (f x) + n * (f y) ) 
          = abs ( f (x^n) + n * (f y)) -- f is homogeneous (on x)
        : by {congr' 2, rw f_is_hmg} 
      ... = abs ( f (x^n) + f(y^n) ) -- f is homogeneous (on y)
        : by {congr' 2, rw f_is_hmg}  
      ... = abs(- f (x^n) - f(y^n) ) 
        : by {simp only[inv_pow, y], rw ←abs_neg, congr' 1, linarith}
      ... = abs (f (x^n * y^n) - f(x^n) - f(y^n) - f (x^n * y^n)) 
        : by {congr' 1, linarith}
      ... ≤ abs (f (x^n * y^n) - f(x^n) - f(y^n)) + abs (-f (x^n * y^n)) 
        : by {apply abs_add _ _} 
      ... ≤ c + abs (f 1) -- f is a quasimorphism
        : by {apply add_le_add, apply mult_def_f (x^n) (y^n), simp},
    end,

    -- and now the actual estimate
    calc  abs(f x + f y)
        = abs((n * f x + n * f y) / n)
        : by {congr' 1, field_simp[n_non0], linarith} 
    ... = abs(n * f x + n * f y) / n
        : by {rw abs_div, simp}    
    ... ≤ (c + abs(f 1)) / n
        : by {apply div_le_div_of_le, simp only[n_pos,nat.cast_nonneg], exact n_fold_est}
  end,
  -- ... and thus f(x) + f(x^-1) must be equal to 0
  have sum_est: f x + f(x⁻¹) = 0,
       by exact zero_via_1_over_n (f x + f(x⁻¹)) (c + abs (f 1)) (abs_sum_est),
  show f(x⁻¹) = - (f x), 
       by linarith,     
end
